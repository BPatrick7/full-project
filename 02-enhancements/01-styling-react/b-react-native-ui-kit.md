# OPTIONAL: Better Styles for React Native Web

I really don't want to dabble with CSS, and I'm ok with my app looking completely generic. I will use a set of pre-prepared components that style React Native Web.

There are a few, like [Nachos UI](https://avocode.com/nachos-ui/), [NativeBase](https://nativebase.io/). Many more, like [Native UI Kitten](https://akveo.github.io/react-native-ui-kitten/) or [React Native Elements](https://react-native-training.github.io/react-native-elements/), provide a set of components, but for `React Native` only; we need our components to work on the web too.  
The most used is [React Native Elements](https://react-native-training.github.io/react-native-elements), but like many others, it needs us to eject out of `create-react-app`, losing all the simplicity this tool gives us.

Instead, I've opted for [Nachos UI](https://avocode.com/nachos-ui/), which is also, in my opinion, nicer to look at.

```sh
npm install --save nachos-ui
```

To use the Nacho theme, we need to import something Nacho calls a `ThemeProvider`, and wrap our application with it.

Let's try it; open `App.js`, and change it for:

```js
import React from 'react';
import { ThemeProvider, Button } from "nachos-ui";
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  box: { padding: 10 },
  text: { fontWeight: 'bold' }
});

class App extends React.Component {
  render() {
    return (
      <ThemeProvider>
        <View style={styles.box}>
          <Text style={styles.text}>Hello, world!</Text>
          <Button>This is a button</Button>
        </View>
      </ThemeProvider>
    );
  }
}

export default App
```

Run the app with `npm start` (or `npm run front` from the root)

Congrats, you have a theme. To know what you can use, refer to it's [documentation](https://avocode.com/nachos-ui/docs/), as well as the regular [React Native](https://facebook.github.io/react-native/docs/components-and-apis.html) components. To see how they look on the web, you can check [React Web Native examples](https://necolas.github.io/react-native-web/examples/) and [React Native Storybook](https://necolas.github.io/react-native-web/storybook).