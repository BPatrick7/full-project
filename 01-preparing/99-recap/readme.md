## Enjoy

You have a fully functional base upon which to build applications with a back-end, and a front-end.

You could save this in a repo and use it as a starter kit whenever you need such a setup