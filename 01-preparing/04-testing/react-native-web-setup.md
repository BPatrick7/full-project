# If you use React Native Web

You will get an error due to `react-native-web` needing canvas.

We'll fix it by providing a false function to the tests. create a file called `setupTests.js` in  `front/src`, and write in it:

```js
// front/src/setupTests.js
HTMLCanvasElement.prototype.getContext = () => ({})
```

This creates a function that returns nothing, replacing the native function needed. It is used for testing only.

**note**: `setupTests.js` is used by the test suite of `create-react-app`. It is not a generic way of setting tests up. It's *only* for `create-react-app`.

run the tests again, and they should pass without warnings.
