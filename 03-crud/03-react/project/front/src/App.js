import React, { Component } from "react";
import Contact from "./Contact";
import "./App.css";

class App extends Component {
  state = {
    contacts_list: [],
    error_message: "",
    name: "",
    email: ""
  };
  getContact = async id => {
    // check if we already have the contact
    const previous_contact = this.state.contacts_list.find(
      contact => contact.id === id
    );
    if (previous_contact) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/contacts/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const contact = answer.result;
        const contacts_list = [...this.state.contacts_list, contact];
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteContact = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/contacts/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const contacts_list = this.state.contacts_list.filter(
          contact => contact.id !== id
        );
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateContact = async (id, props) => {
    try {
      if (!props || !(props.name || props.email)) {
        throw new Error(
          `you need at least name or email properties to update a contact`
        );
      }
      const response = await fetch(
        `http://localhost:8080/contacts/update/${id}?name=${props.name}&email=${
          props.email
        }`
      );
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const contacts_list = this.state.contacts_list.map(contact => {
          // if this is the contact we need to change, update it. This will apply to exactly
          // one contact
          if (contact.id === id) {
            const new_contact = {
              id: contact.id,
              name: props.name || contact.name,
              email: props.name || contact.email
            };
            return new_contact;
          }
          // otherwise, don't change the contact at all
          else {
            return contact;
          }
        });
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createContact = async props => {
    try {
      if (!props || !(props.name && props.email)) {
        throw new Error(
          `you need both name and email properties to create a contact`
        );
      }
      const { name, email } = props;
      const response = await fetch(
        `http://localhost:8080/contacts/new/?name=${name}&email=${email}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const contact = { name, email, id };
        const contacts_list = [...this.state.contacts_list, contact];
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getContactsList = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/contacts/list?order=${order}`
      );
      const answer = await response.json();
      if (answer.success) {
        const contacts_list = answer.result;
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  componentDidMount() {
    this.getContactsList();
  }
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and email from state
    const { name, email } = this.state;
    // create the contact from mail and email
    this.createContact({ name, email });
    // empty name and email so the text input fields are reset
    this.setState({ name: "", email: "" });
  };
  render() {
    const { contacts_list, error_message } = this.state;
    return (
      <div className="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}
        {contacts_list.map(contact => (
          <Contact
            key={contact.id}
            id={contact.id}
            name={contact.name}
            email={contact.email}
            updateContact={this.updateContact}
            deleteContact={this.deleteContact}
          />
        ))}
        <form className="third" onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="name"
            onChange={evt => this.setState({ name: evt.target.value })}
            value={this.state.name}
          />
          <input
            type="text"
            placeholder="email"
            onChange={evt => this.setState({ email: evt.target.value })}
            value={this.state.email}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
      </div>
    );
  }
}

export default App;
