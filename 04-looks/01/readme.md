# Structure

We could be using any number of frameworks, but we wont. In order to understand css better, and also because it's not so hard, we'll build the style from scratch.

If you didn't want to do so from scratch, here's a selection of simple, yet elegant CSS frameworks you could choose to use. Each of the links below leads to a *pure css* framework, without any javascript usage. The ones that are tagged as `minimalistic` work with very few classes.

- Very simple frameworks
  - [Chota](https://jenil.github.io/chota/) (Grid, UI, minimalistic)
  - [Picnic](https://picnicss.com/)★★ (Grid, UI, minimalistic) 
  - [Tacit](https://yegor256.github.io/tacit/) (UI, minimalistic)
  - [Wing](https://kbrsh.github.io/wing/)★  (Grid, UI, minimalistic)
  - [Frow](https://frowcss.com/grid-system.html)★ (Grid, UI, Sass)
- Complete Frameworks
  - [Pure](https://purecss.io/)★ (Complete yet Simple)
  - [TurretCss](https://turretcss.com/)★ (Complete yet simple)
  - [Bulma](https://bulma.io) (Complete, Sass)★ 
  - [Fictoan](https://sujan-s.github.io/fictoan/)★★  (Complete, Sass)
  - [Material Design Light](https://getmdl.io/components/index.html) (Complete, Sass)
  - [Vital](https://vitalcss.com/) (Complete, Sass)★ 
  - [Look](https://rawgit.com/box2unlock/look/master/index.html)★ (Typography, UI, Sass)
  - [InvisCss](https://cmroanirgo.github.io/inviscss/demo/) (Grid, UI, Less)
  - [Vanilla](https://vanillaframework.io/) (Complete, Sass)
- In-between
  - [Spark](https://www.codewithspark.com/) (Grid, UI, Less)
  - [Schema UI](http://danmalarkey.github.io/schema/) (Grid, UI, Less)
  - [Concise](http://concisecss.com) (Grid, UI, Sass)
  - [Blaze UI](https://www.blazeui.com/) (Grid, UI)
  - [Mini](https://minicss.org) (Grid, UI)
  - [Mobi](http://getmobicss.com/) (Grid, UI)
  - [Kouto Swiss](http://kouto-swiss.io/) (Framework, Grid, Stylus)
  - [Avalanche](https://avalanche.oberlehner.net/) (Framework, Sass)

Bear in mind that, at the time of writing, `create-react-app` has native support for SASS but not for LESS. You can still, of course, use the generated CSS for any framework, but still something to bear in mind.

In our case, we will use none of those (we will also remove `picnic` in a moment), but we will observe them and copy parts of code for our own benefit.

Take time to open some of those links and look at the elements of those different libraries

## SASS

In order to write our stylesheets, we will use `Sass`. `Sass` is a simple language that compiles to css. It allows to use variables, calculations, `import`s and nesting.

Support is already there for `create-react-app`, but we need to install the compiler:

```sh
npm install --save node-sass
```

You now need to choose which "flavor" of sass you want to use. Sass can read two types of files: `.sass` and `.scss`.

Here's a `.scss` file:

```scss
// some-file.scss
$font-stack: Helvetica, sans-serif;
$primary-color: #333;

.some-class {
  font: 100% $font-stack;
  color: $primary-color;
  .inside-class { display: inline-block; }
}
```

This would compile to:

```css
/** some-file.css **/
.some-class {
  font: 100% Helvetica, sans-serif;
  color: #333;
}
.some-class .inside-class { display: inline-block; }
```

The same file, written in `sass`, would read:

```sass
// some-file.sass
$font-stack: Helvetica, sans-serif
$primary-color: #333

.some-class
  font: 100% $font-stack
  color: $primary-color
  .inside-class
    display: inline-block
```

As you see, Sass is cleaner (depending on your tastes), but you lose the ability to be able to copy-paste code directly from a css file.

For this reason, most people choose the `scss` format. That's what we'll go with too.

rename `front/src/index.css` to `front/src/index.scss`.

Also change the `import './index.css'` line in `front/src/index.js` to `import './index.scss'`.
Also remove the `import "picnic/picnic.min.css"` line. We will write our own styles. 

You can later remove picnic by going to `front` and running

```sh
npm remove --save picnic
```

You've set up Sass!

## Design

Since we're not designers however, we need some sort of guideline. Here's what I chose:

![a screenshot of the Cloze App](./cloze-iphone-screenshot.jpg)  
*This is the Cloze app, a contact and calendar application for IOS*

We don't have the same features, so obviously we won't do a 1:1 copy. We also wouldn't want to do that. But we'll follow general ideas and spacing.

For naming our classes, we'll follow the [BEM](http://getbem.com/naming/) methodology. It's pretty simple. Let's say you have a navigation element. You might call it `main-nav`. It contains links, which will be `main-nav__link`. If say, one link is special, it would be called `main-nav__link--special`. That's it! That's BEM: `element__sub-element--modifier`.

We'll go with mobile-first, as obviously implied by the design.

## General layout

First, we will create:
  - a grid system
  - some utility classes such as [the media element](http://www.stubbornella.org/content/2010/06/25/the-media-object-saves-hundreds-of-lines-of-code/ ), and a loading spinner

### Grid

We need:
  - one element that prevents the site from growing too much on large screens. We'll call that a `.wrapper`.
    - `.wrapper` will limit of wide the website can grow
  - one element that creates columns inside of itself and distributes the elements. We'll call that `.container`
    - `.container` will use `flex` to distribute items in columns
  - we'll also have a `.container-wrapper`, which will do both
    - we often will have items that should have limited width, but also internal columns
  - we'll have some pre-defined columns sizes
    - items inside `.container` will use as much space as they can, but we will have classes like `col-1`, `col-2` and so on, which we can use to precisely set the width of columns with 

We will use flexbox, which is not [supported by all browsers](https://caniuse.com/#feat=flexbox), but still supported by *95%* of browsers out there.

Here are a few references that you can use to build a flexbox grid.
- [CSS Tricks Flexbox Article](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [CSS Tricks Article for Building a Simple Flexbox Grid](https://css-tricks.com/dont-overthink-flexbox-grids/)
- [Philip Walton's Flexbox Grid](https://philipwalton.github.io/solved-by-flexbox/demos/grids/)
- [SimpleGrid Framework](https://simplegrid.io/)
- [GridLex Framework](http://gridlex.devlint.fr/)

Using Flex is extremely easy. Here's how you build a flex box:

```css
.container{
  display: flex;
}
.container-column{
  flex: 1
}
```

https://codepen.io/Xananax/pen/PxMxKx

// phone and + (576+)
@media (min-width: 36em){}

// tablet and + (768px)
@media (min-width: 48em){}

// small laptop and + (1024px)
@media (min-width: 64em){}

// laptop and + (1280px)
@media (min-width: 80em){}


## Placing Classes

Let's first put the classes we might need everywhere we might need them.

Obviously, our contact link is currently very simple, compared to the layout above. Let's replace it with this new file:

```js
// front/stc/ContactLink.js
import React from 'react'
import { Link } from "react-router-dom";

const ContactLink = ({ id, name, email, image, style }) => (
  <div className="contact-link" style={style}>
    {image ? (
      <img
        className="contact-link__avatar-image"
        src={`//localhost:8080/images/${image}`}
        alt={`the avatar of ${name}`}
      />
    ) : (
      <div className="contact-link__avatar-letter">
        <span>{name[0].toUpperCase()}</span>
      </div>
    )}
    <Link className="contact-link__text" to={"/contact/" + id}>
      <span className="contact-link__text__name">{name}</span>
      <span className="contact-link__text__email">{email}</span>
    </Link>
  </div>
);

export default ContactLink
```

then replace the previous link in `ContactList.js` (and also add a `contact-list` wrapper):

```js
// front/src/ContactList.js
...
import ContactLink from './ContactLink'

const ContactList = ({ contacts_list }) => (
  <div className="contact-list">
    <Transition
      ...
    >
      {contact => style => <ContactLink style={style} {...contact} />}
    </Transition>
  </div>
);
...
```

Let's give some structure to the app by putting our menu in a `header` and the rest of the page in a `body`. Also, let's add some classes to the menu:

```js
// front/src/App.js
class App extends Component {
  ...
  render() {
    return (
      <div className="App">
        <div className="header">
          <div className="main-nav">
            <Link className="main-nav__link" to="/">Home</Link>
            <Link className="main-nav__link" to="/profile">profile</Link>
            <IfAuthenticated>
              <Link className="main-nav__link" to="/create">create</Link>
            </IfAuthenticated>
          </div>
        </div>
        <div className="body">
          {this.renderContent()}
        </div>
        <ToastContainer />
      </div>
    );
  }
  ...
}
```

We'll also add a (currently non-functional) search box at the top.

Inside `header`, but before `main-nav`, insert the following:

```js
...
  render() {
    return (
      <div className="App">
          ...
          <div className="search">
            <input className="search__input" type="text"/>
            <button className="search__button">search</button>
          </div>
          <div className="main-nav">
          ...
...
```

Finally, we'll modify `Contact.js`:

```js
// front/src/Contact.js
...
  render() {
    const { editMode } = this.state;
    return (
      <div className="contact--page">
        {editMode ? this.renderEditMode() : this.renderViewMode()}
      </div>
    );
  }
...
```

This is the structure we now have:

```jade
#App
  .header
    .search
      .search__input
      .search__button
    .main-nav
      .main-nav__link
  .body
    .contact-list
      .contact-link
        .contact-link__avatar-image
        .contact-link__avatar-letter
        .contact-link__text
          .contact-link__text__name
          .contact-link__text__email
    .contact-page
```

Open `index.css` and dump this in it:

```css
#App{}
.header{}
.search{}
.search__input{}
.search__button{}
.main-nav{}
.main-nav__link{}
.body{}
.contact-list{}
.contact-link{}
.contact-link__avatar-image{}
.contact-link__avatar-letter{}
.contact-link__text{}
.contact-link__text__name{}
.contact-link__text__email{}
.contact-page{}
```

We will probably have to intervene on the html structure again, but we have enough detail to begin styling.
